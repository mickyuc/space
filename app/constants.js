export const PAGELEN = 10;
// export const BACKEND_URL = 'https://cryptoonline.ml:3000/';
//export const BACKEND_URL = 'http://192.168.8.55:3000/';
//export const BACKEND_URL = 'https://spare-space.co.kr:3000/';
//export const BACKEND_URL = 'https://space.asfd.online:3000/';
export const BACKEND_URL = 'https://newsp.spare-space.co.kr:3000/';

export const SERVICE_IMGS = [
    'https://spare-space.co.kr/space/s_manual_1.png'  ,
    'https://spare-space.co.kr/space/s_manual_2.png'  ,
    'https://spare-space.co.kr/space/s_manual_3.png'  ,
    'https://spare-space.co.kr/space/s_manual_4.png'  ,
    'https://spare-space.co.kr/space/s_manual_5.png'  ,
    'https://spare-space.co.kr/space/s_manual_6.png'  ,
    'https://spare-space.co.kr/space/s_manual_7.png'  ,
    'https://spare-space.co.kr/space/s_manual_8.png'  ,
    'https://spare-space.co.kr/space/s_manual_9.png'  ,
    'https://spare-space.co.kr/space/s_manual_10.png' ];

export const PRICE_MAIN = 10000;                        // 기본비용
export const PRICE_BOX_PER_MONTH = 10000;               // 박스 1개 / 월
export const PRICE_DELIVERY_OF_RETURN_GOOD = 5000;      // 개별물건찾기시 택배비용
export const PRICE_RESTORE = 10000;                     // 기간내 재보관
export const PRICE_SERVICE_USE = 10000;                 // 개별찾기시 서비스 이용비용
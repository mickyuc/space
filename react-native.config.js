module.exports = {
    dependencies: {
      'iamport-react-native': {
        platforms: {
          android: null, // disable Android platform, other platforms will still autolink
        },
      },
      'react-native-webview': {
        platforms: {
          android: null, // disable Android platform, other platforms will still autolink
        },
      },
    },
  };
//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Colin K on 10/29/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
